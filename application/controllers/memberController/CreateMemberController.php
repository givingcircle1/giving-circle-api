<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class CreateMemberController extends REST_Controller {
  protected $method = null;
  protected $date=null;
  function __construct()
  {
    // Construct the parent class
    parent::__construct();
    $this->load->model('memberModel/CreateMemberModel','create');
    $this->load->model('commonModel/HelperModel','custom');
    $this->method = $_REQUEST;
    $this->date=date('Y-m-d H:i:s');
  }


  // Function for create circle
  public function add_member_in_circle_post(){
    $output = array();
    $headers = (object)$this->input->request_headers();
    $where_array = array('user_id' => $headers->user_id,'user_token' => $headers->token);
    $res = $this->custom->checkExis('user_id','tbl_user',$where_array);
    if ($res == false) {
    $output = array();
    $output['status'] = false;
    $output['msg'] = 'Invalid token or user id.!';
    $this->set_response($output, REST_Controller::HTTP_OK);
  }else{
    // $circle_data = (object)$this->input->post();
    $circle_data = (object) json_decode($this->security->xss_clean( $this->input->raw_input_stream ), true);
      $post_data = array(
       'user_id' => $circle_data->user_id,
       'role_id' => $circle_data->role_id,
       'circle_id' =>$circle_data->circle_id,
       'is_active' => $circle_data->is_active,
       'created_on' => $this->date,
       'created_by' => $headers->user_id
     );
     $res = $this->create->add_members_in_circle($post_data);
     if ($res != false && $res != null) {
        $output['status'] = true;
        $output['msg'] = 'Member added successfully.!';
        $output['member_data'] = $res;
    }else{
      $output['status'] = false;
      $output['msg'] = 'Failed to add Member, please try again.!';
    }
  }
  $this->set_response($output, REST_Controller::HTTP_OK); // CREATED (200) being the HTTP response code
}


}//class closing
