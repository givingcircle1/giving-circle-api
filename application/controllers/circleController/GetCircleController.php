<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class GetCircleController extends REST_Controller {
  function __construct()
  {
    // Construct the parent class
    parent::__construct();
    $this->load->model('circleModel/GetCircleModel','get');
    $this->load->model('commonModel/HelperModel','custom');
    $this->load->model('commonModel/GetModel','common');
    $method = $_REQUEST;
    // $header = (object)$this->input->request_headers();
  }

// Function for get all circles
public function all_circle_get(){
  $headers = (object)$this->input->request_headers();
  $where_array = array('user_id' => $headers->user_id,'user_token' => $headers->token);
  $res = $this->custom->checkExis('user_id','tbl_user',$where_array);
  if ($res == false) {
  $output = array();
  $output['status'] = false;
  $output['msg'] = 'Invalid token or user id.!';
  $this->set_response($output, REST_Controller::HTTP_OK); // Get 200 ok response
}else{
  $data = $this->get->get_circle_list();
  $output = array();
  if ($data != false) {
    $output['status'] = true;
    $output['msg'] = 'All circles fetch successfully.!';
    $output['all_circles'] = $data;
  }else{
    $output['status'] = false;
    $output['msg'] = 'No circles available.!';
  }
  $this->set_response($output, REST_Controller::HTTP_OK); // Get 200 ok response
}
}

// Function for get members by circle
public function members_by_circle_get(){
  $headers = (object)$this->input->request_headers();
  $where_array = array('user_id' => $headers->user_id,'user_token' => $headers->token);
  $res = $this->custom->checkExis('user_id','tbl_user',$where_array);
  if ($res == false) {
  $output = array();
  $output['status'] = false;
  $output['msg'] = 'Invalid token or user id.!';
  $this->set_response($output, REST_Controller::HTTP_OK); // Get 200 ok response
}else{ 
  $circle_data = (object)$this->input->get();
  $data = $this->get->get_circle_by_circle_id($circle_data->circle_id);
  $output = array();
  if ($data != false) {
    $members = $this->common->get_user_list($circle_data->circle_id);
    $output['status'] = true;
    $output['msg'] = 'circle fetch successfully.!';
    $output['circle'] = $data;
    if ($members != false) {
    $output['members'] = $members;
    }else{
    $output['members'] = "No members found.!";
    }
  }else{
    $output['status'] = false;
    $output['msg'] = 'No circle available.!';
    // $output['id'] = $data;
  }
  $this->set_response($output, REST_Controller::HTTP_OK); // Get 200 ok response
}
}

// Function for get all circles by member
public function circle_by_member_get(){
  $headers = (object)$this->input->request_headers();
  $where_array = array('user_id' => $headers->user_id,'user_token' => $headers->token);
  $res = $this->custom->checkExis('user_id','tbl_user',$where_array);
  if ($res == false) {
  $output = array();
  $output['status'] = false;
  $output['msg'] = 'Invalid token or user id.!';
  $this->set_response($output, REST_Controller::HTTP_OK); // Get 200 ok response
}else{
  $member_data = (object)$this->input->get();
  $data = $this->get->get_member_details($member_data->member_id);
  // print_r($data);
  // die();
  $output = array();
  if ($data != false) {
    $circle = $this->get->get_circle_by_member_id($member_data->member_id);
    $output['status'] = true;
    $output['msg'] = 'Members circles fetch successfully.!';
    $output['circle'] = $data;
    if ($circle != false) {
    $output['circle'] = $circle;
    }else{
    $output['circle'] = "No Members circle found.!";
    }
  }else{
    $output['status'] = false;
    $output['msg'] = 'No members circle available.!';
  }
  $this->set_response($output, REST_Controller::HTTP_OK); // Get 200 ok response
}
}


}//class closing
