<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class GetActivitiesController extends REST_Controller {

  function __construct()
  {
    // Construct the parent class
    parent::__construct();
    $this->load->model('activitiesModel/GetActivitiesModel','get');
    $this->load->model('commonModel/HelperModel','custom');
    $method = $_REQUEST;
    // $header = (object)$this->input->request_headers();
  }

  // Function for get all activities by circle
  public function activities_by_circle_get(){
    $headers = (object)$this->input->request_headers();
    $where_array = array('user_id' => $headers->user_id,'user_token' => $headers->token);
    $res = $this->custom->checkExis('user_id','tbl_user',$where_array);
    if ($res == false) {
    $output = array();
    $output['status'] = false;
    $output['msg'] = 'Invalid token or user id.!';
    $this->set_response($output, REST_Controller::HTTP_OK);
  }else{
    $circle_data = (object)$this->input->get();
    $data = $this->get->get_circle_by_id($circle_data->circle_id);
    $output = array();
    if ($data != false) {
      $activity = $this->get->get_activities($circle_data->circle_id);
      $output['status'] = true;
      $output['msg'] = 'Circle fetch successfully.!';
      $output['circle'] = $data;
      if ($activity != false) {
      $output['activity'] = $activity;
      }else{
      $output['activity'] = "No activities found.!";
      }
    }else{
      $output['status'] = false;
      $output['msg'] = 'No circle available.!';
      // $output['id'] = $data;
    }
    $this->set_response($output, REST_Controller::HTTP_OK);
  }
  }


}//class closing
