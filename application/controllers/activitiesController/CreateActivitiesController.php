<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';
class CreateActivitiesController extends REST_Controller {
  protected $method = null;
  protected $date=null;
  function __construct()
  {
    // Construct the parent class
    parent::__construct();
    $this->load->model('activitiesModel/CreateActivitiesModel','create');
    $this->load->model('activitiesModel/moneyModel/CreateMoneyModel','money_create');
    $this->load->model('activitiesModel/timeModel/CreateTimeModel','time_create');
    $this->load->model('activitiesModel/kindModel/CreateKindModel','kind_create');
    $this->load->model('commonModel/HelperModel','custom');
    $this->method = $_REQUEST;
    $this->date=date('Y-m-d H:i:s');
  }

  // Function for add activity in circle by circle_id
  public function create_activity_post(){
    $output = array();
    $headers = (object)$this->input->request_headers();
    $where_array = array('user_id' => $headers->user_id,'user_token' => $headers->token);
    $res = $this->custom->checkExis('user_id','tbl_user',$where_array);
    if ($res == false) {
    $output = array();
    $output['status'] = false;
    $output['msg'] = 'Invalid token or user id.!';
    $this->set_response($output, REST_Controller::HTTP_OK); // CREATED (200) being the HTTP response code
  }else{
    // $activity_data = (object)$this->input->post();
    $activity_data = (object) json_decode($this->security->xss_clean( $this->input->raw_input_stream ), true);
      $post_data = array(
       'activity_name' => $activity_data->activity_name,
       'circle_id' => $activity_data->circle_id,
       'type_id' =>$activity_data->type_id,
       'start_date' => $activity_data->start_date,
       'end_date' => $activity_data->end_date,
       'location' => $activity_data->location,
       'need_collections' => $activity_data->need_collections,
       'kind' => $activity_data->kind,
       'time' => $activity_data->time,
       'money' => $activity_data->money,
       'latitude' => $activity_data->latitude,
       'longitude' => $activity_data->longitude,
       'pin_code' => $activity_data->pin_code,
       'created_on' => $this->date,
       'created_by' => $headers->user_id
     );

     $res = $this->create->createNewActivity($post_data);
     if ($res != false && $res != null) {
       $output['status'] = true;
       $output['msg'] = 'Activity added successfully.!';
       $output['activity_id'] = $res;
      // $today = date('Y-m-d');
      // Insert data in money giving by activity_id
       if($activity_data->money == 1){
        $money_data = array('activity_id'=>$res,'member_id' =>$headers->user_id, 'mg_date'=>$activity_data->mg_date, 'amount' =>$activity_data->money, 'transaction_no'=>$activity_data->transaction_no, 'make_donation_public'=>$activity_data->make_donation_public, 'created_on'=>$this->date, 'created_by'=>$headers->user_id);
        $money_res = $this->money_create->add_money($money_data);
        if($money_res != false && $money_res != null){
          $output['money_status'] = true;
          $output['money_msg'] = 'Money giving added successfully.!';
          // $output['mg_id'] = $money_res;
        }
        else{
          $output['money_status'] = false;
          $output['money_msg'] = 'Failed to add money giving, please try again.!';
        }
       }
      
       // Insert data in time giving by activity_id
       if($activity_data->time == 1){
        $time_data = array('activity_id'=>$res,'member_id' =>$headers->user_id, 'tg_date'=>$activity_data->tg_date, 'start_time' =>$activity_data->start_time, 'end_time'=>$activity_data->end_time, 'created_on'=>$this->date, 'created_by'=>$headers->user_id);
        $time_res = $this->time_create->add_time($time_data);
        if($time_res != false && $time_res != null){
          $output['time_status'] = true;
          $output['time_msg'] = 'Time giving added successfully.!';
          // $output['time_id'] = $time_res;
        }
        else{
          $output['time_status'] = false;
          $output['time_msg'] = 'Failed to add time giving, please try again.!';
        }
       }
       
       // Insert data in kind giving by activity_id
       if($activity_data->kind == 1){
        $kind_data = array('activity_id'=>$res,'member_id' =>$headers->user_id, 'collection_id'=>$activity_data->collection_id, 'kg_desc' =>$activity_data->kg_desc, 'created_on'=>$this->date, 'created_by'=>$headers->user_id);
        $kind_res = $this->kind_create->add_kind($kind_data);
        if($kind_res != false && $kind_res != null){
          $output['kind_status'] = true;
          $output['kind_msg'] = 'Kind giving added successfully.!';
        }
        else{
          $output['kind_status'] = false;
          $output['kind_msg'] = 'Failed to add kind giving, please try again.!';
        }
       }

    }else{
      $output['status'] = false;
      $output['msg'] = 'Failed to add activity, please try again.!';
    }
  }
  $this->set_response($output, REST_Controller::HTTP_OK); // CREATED (200) being the HTTP response code
  }

  // Function for add Collection in given activity
   public function add_collection_post(){
    $output = array();
    $headers = (object)$this->input->request_headers();
    $where_array = array('user_id' => $headers->user_id,'user_token' => $headers->token);
    $res = $this->custom->checkExis('user_id','tbl_user',$where_array);
    if ($res == false) {
    $output = array();
    $output['status'] = false;
    $output['msg'] = 'Invalid token or user id.!';
    $this->set_response($output, REST_Controller::HTTP_OK); // CREATED (200) being the HTTP response code
  }else{
    // $collection_data = (object)$this->input->post();
    $collection_data = (object) json_decode($this->security->xss_clean( $this->input->raw_input_stream ), true);
      $post_data = array(
       'activity_id' => $collection_data->activity_id,
       'time_slot' => $collection_data->time_slot,
       'start_time' =>$collection_data->start_time,
       'end_time' => $collection_data->end_time,
       'collector_id' => $headers->user_id,
       'created_on' => $this->date,
       'created_by' => $headers->user_id
     );
     
     $res = $this->create->add_new_collection($post_data);
     if ($res != false && $res != null) {
       $output['status'] = true;
       $output['msg'] = 'Collection added successfully.!';
       $output['collection_id'] = $res;
      }else{
        $output['status'] = false;
        $output['msg'] = 'Failed to add collection, please try again.!';
      }
    }
    $this->set_response($output, REST_Controller::HTTP_OK); // CREATED (200) being the HTTP response code
}



}//class closing
