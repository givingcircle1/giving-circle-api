<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class CreateMoneyController extends REST_Controller {
  protected $method = null;
  protected $date=null;
  function __construct()
  {
    // Construct the parent class
    parent::__construct();
    $this->load->model('activitiesModel/moneyModel/CreateMoneyModel','create');
    $this->load->model('commonModel/moneyModel/HelperModel','custom');
    $this->method = $_REQUEST;
    $this->date=date('Y-m-d H:i:s');
  }




}//class closing
