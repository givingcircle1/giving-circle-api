<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';
/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class CreateController extends REST_Controller {
  protected $method = null;
  protected $date=null;
  function __construct()
  {
        // Construct the parent class
        parent::__construct();
        // $this->methods['users_get']['limit'] = 500; // 500 requests per hour per user/key
        // $this->methods['users_post']['limit'] = 100; // 100 requests per hour per user/key
        // $this->methods['users_delete']['limit'] = 50; // 50 requests per hour per user/key
        $this->load->model('CreateModel','create');
        $this->load->model('HelperModel','custom');
        $this->method = $_REQUEST;
        $this->date=date('Y-m-d H:i:s');
      }

      public function user_register_post(){
        $output = array();
        $user = (object)$this->input->post();
        $token = $this->custom->genToken();
        $user_data = array(
         'invitation_id' => $user->invitation_id,
         'user_name' => $user->user_name,
        //  'password' => md5($user->password),
         'user_token' => $token,
         'f_name' => $user->f_name,
         'm_name' => $user->m_name,
         'l_name' => $user->l_name,
         'address' => $user->address,
         'city_id' => $user->city_id,
         'locality' => $user->locality,
         'mobile' => $user->mobile,
         'email' => $user->email,
         'app_id' => $user->app_id,
         'mac_id' => $user->mac_id,
         'created_on' => $this->date
       );
        $res = $this->create->register_user($user_data);
        if ($res != false) {
          $output['status'] = true;
          $output['msg'] = 'User Registered Successfully.!';
          $output['user_data'] = $res;
        }else{
          $output['status'] = false;
          $output['msg'] = 'Registration failed, please try again.!';
        }
        $this->set_response($output, REST_Controller::HTTP_OK); // CREATED (200) being the HTTP response code
      }

      // Function for user mobile number verify first time
      public function user_mobile_verify_post(){
        $output = array();
        $user = (object)$this->input->post();
        $output = array();
        $arr= array(
         array(
           'field'=>'mobile',
           'label'=>'',
           'rules'=>'required|min_length[10]|max_length[10]|regex_match[/^[0-9]{10}$/]|is_unique[tbl_user.mobile]',
           'errors'=>array('required'=>'mobile number is required','min_length'=>'Minimum length of mobile number is 10','max_length'=>'Max length of mobile number is 10','regex_match' => 'Please enter valid mobile number','is_unique'=>'ohh, given mobile number is already exists')
         )
       );

        $this->form_validation->set_rules($arr);
        if ($this->form_validation->run()==false){
         $output['status'] = false;
       // $output['msg'] = $this->form_validation->error_array();
         $output['msg'] = $this->form_validation->error_array()['mobile'];
        $this->set_response($output, REST_Controller::HTTP_OK); // CREATED (200) HTTP response code
      }else{
        $res = $this->custom->send_otp($user->mobile);
        $this->set_response($res, REST_Controller::HTTP_OK); // CREATED (200) being the HTTP response code
      }
    }

// Function for create circle
    public function create_circle_post(){
      $output = array();
      $headers = (object)$this->input->request_headers();
      $where_array = array('user_id' => $headers->user_id,'user_token' => $headers->token);
      $res = $this->custom->checkExis('user_id','tbl_user',$where_array);
      if ($res == false) {
      $output = array();
      $output['status'] = false;
      $output['msg'] = 'Invalid token or user id.!';
      $this->set_response($output, REST_Controller::HTTP_OK);
    }else{
      $circle_data = (object)$this->input->post();
        $post_data = array(
         'circle_name' => $circle_data->circle_name,
         'type_id' => $circle_data->type_id,
         'is_open' =>$circle_data->is_open,
         'can_member_invite' => $circle_data->can_member_invite,
         'vision' => $circle_data->vision,
         'mission' => $circle_data->mission,
         'purpose' => $circle_data->purpose,
         'created_on' => $this->date,
         'created_by' => $headers->user_id
       );
       $res = $this->create->circle_create($post_data);
       if ($res != false && $res != null) {
          $output['status'] = true;
          $output['msg'] = 'Circle created successfully.!';
          $output['circle_data'] = $res;
      }else{
        $output['status'] = false;
        $output['msg'] = 'Circle creation failed, please try again.!';
      }
    }
    $this->set_response($output, REST_Controller::HTTP_OK); // CREATED (200) being the HTTP response code
  }


}//class closing
