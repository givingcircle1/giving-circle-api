<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class GetController extends REST_Controller {

  function __construct()
  {
    // Construct the parent class
    parent::__construct();
        // $this->methods['users_get']['limit'] = 500; // 500 requests per hour per user/key
        // $this->methods['users_post']['limit'] = 100; // 100 requests per hour per user/key
        // $this->methods['users_delete']['limit'] = 50; // 50 requests per hour per user/key
    $this->load->model('GetModel','get');
    $this->load->model('HelperModel','custom');
    $method = $_REQUEST;
    // $header = (object)$this->input->request_headers();
  }

// Get login user
public function log_send_user_otp_get() {
   $output = array();
   $login_data = (object)$this->input->get();
   // $data= array('user_name' =>$login_data->username, 'password' =>md5($login_data->password));
   $data= array('mobile' =>$login_data->mobile);

   $res=$this->get->user_login($data);
   if($res != NULL && count($res) > 0 && $res != false) {
    // send otp for login user
    $sended_otp = $this->custom->send_otp($login_data->mobile);

  $this->set_response($sended_otp, REST_Controller::HTTP_OK); // returns 200 OK
}else{
  $output['status'] = false;
  $output['msg'] = "Given mobile number is not registered.!";
  $this->set_response($output, REST_Controller::HTTP_OK); // returns 200 OK
}
}

// Function for verify OTP
public function verify_otp_get() {
 $output = array();
 $login_data = (object)$this->input->get();
 $where_array= array('otp_id' =>$login_data->otp_id,'otp_no'=>$login_data->otp,'status'=>0 );
 $res=$this->custom->get_limited('mobile_no', "tbl_otp", $where_array, 1);

 if($res != NULL && count($res) > 0 && $res != false) {
    // send otp for login user
  $user_data = $this->get->otp_status_update($where_array,$res[0]['mobile_no']);
  if ($user_data != false && count($user_data) > 0) {
    $output['status'] = false;
    $output['msg'] = "OTP has been verified successfully.!";
    $output['user_data'] =$user_data;
  }else{
    $output['status'] = false;
    $output['msg'] = "Something wents wrong, please try again.!";
    $output['user_data'] = $user_data;
  }
  $this->set_response($output, REST_Controller::HTTP_OK); // returns 200 OK
}else{
  $output['status'] = false;
  $output['msg'] = "Oop's invalid OTP.!";
  $this->set_response($output, REST_Controller::HTTP_OK); // returns 200 OK
}
}

// public function log_user_get() {
//    $output = array();
//    $login_data = (object)$this->input->get();
//    // $data= array('user_name' =>$login_data->username, 'password' =>md5($login_data->password));
//    $data= array('mobile' =>$login_data->mobile);

//    $res=$this->get->user_login($data);

//    if($res != NULL && count($res) > 0 && $res != false) {
//     $output['status'] = true;
//     $output['user_id'] = $res[0];
//   }else{
//     $output['status'] = false;
//     $output['msg'] = "Given mobile number is not registered.!";
//   }
//   $this->set_response($data, REST_Controller::HTTP_OK); // returns 200 OK
// }

// Function for get all contries
public function country_get(){
  $data = $this->custom->get_limited("country_id,country_name,country_code", "tbl_country", array(), 2);
  $output = array();
  if ($data != NULL && count($data) > 0) {
    $output['status'] = true;
    $output['msg'] = 'countries found.!';
    $output['countries'] = $data;
  }else{
    $output['status'] = false;
    $output['msg'] = 'No countries available.!';
  }
  $this->set_response($output, REST_Controller::HTTP_OK);
}

// Function for get all states
public function states_get(){
 $data = (object)$this->input->get();
 $data = $this->custom->get_limited("state_id,state_name", "tbl_state", array('country_id'=>$data->country_id), 2);
 $output = array();
 if ($data != NULL && count($data) > 0) {
  $output['status'] = true;
  $output['msg'] = 'states found.!';
  $output['states'] = $data;
}else{
  $output['status'] = false;
  $output['msg'] = 'No states available.!';
}
$this->set_response($output, REST_Controller::HTTP_OK);
}

// Function for get all cities
public function cities_get(){
 $data = (object)$this->input->get();
 $data = $this->custom->get_limited("city_id,city_name", "tbl_city", array('state_id'=>$data->state_id), 2);
 $output = array();
 if ($data != NULL && count($data) > 0) {
  $output['status'] = true;
  $output['msg'] = 'cities found.!';
  $output['cities'] = $data;
}else{
  $output['status'] = false;
  $output['msg'] = 'No cities available.!';
}
$this->set_response($output, REST_Controller::HTTP_OK);
}

// Function for get all users
public function all_user_get(){
  $headers = (object)$this->input->request_headers();
  $where_array = array('user_id' => $headers->user_id,'user_token' => $headers->token);
  $res = $this->custom->checkExis('user_id','tbl_user',$where_array);
  if ($res == false) {
  $output = array();
  $output['status'] = false;
  $output['msg'] = 'Invalid token or user id.!';
  $this->set_response($output, REST_Controller::HTTP_OK);
}else{
  $data = $this->get->get_user_list();
  $output = array();
  if ($data != false) {
    $output['status'] = true;
    $output['msg'] = 'All users fetch successfully.!';
    $output['all_users'] = $data;
  }else{
    $output['status'] = false;
    $output['msg'] = 'No users available.!';
  }

  $this->set_response($output, REST_Controller::HTTP_OK);
}
}


}//class closing
