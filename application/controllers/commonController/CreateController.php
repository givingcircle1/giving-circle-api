<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class CreateController extends REST_Controller {
  protected $method = null;
  protected $date=null;
  function __construct()
  {
    // Construct the parent class
    parent::__construct();
    $this->load->model('commonModel/CreateModel','create');
    $this->load->model('commonModel/HelperModel','custom');
    $this->method = $_REQUEST;
    $this->date=date('Y-m-d H:i:s');
  }

      public function user_register_post(){
        $output = array();
        // $user = (object)$this->input->post();
        $user = (object) json_decode($this->security->xss_clean( $this->input->raw_input_stream ), true);
        $token = $this->custom->genToken();
        $user_data = array(
         'invitation_id' => $user->invitation_id,
         'user_token' => $token,
         'f_name' => $user->f_name,
         'm_name' => $user->m_name,
         'l_name' => $user->l_name,
         'gender' => $user->gender,
         'address' => $user->address,
         'city_id' => $user->city_id,
         'locality' => $user->locality,
         'mobile' => $user->mobile,
         'email' => $user->email,
         'dob' => $user->dob,
         'profession' => $user->profession,
         'latitude' => $user->latitude,
         'longitude' => $user->longitude,
         'pin_code' => $user->pin_code,
         'user_status' => 1,
         'app_id' => $user->app_id,
         'mac_id' => $user->mac_id,
         'created_on' => $this->date
       );
        $res = $this->create->register_user($user_data);
        if ($res != false) {
          $output['status'] = true;
          $output['msg'] = 'User Registered Successfully.!';
          $output['user_data'] = $res;
        }else{
          $output['status'] = false;
          $output['msg'] = 'Registration failed, please try again.!';
        }
        $this->set_response($output, REST_Controller::HTTP_OK); // CREATED (200) being the HTTP response code
      }

      // Function for user mobile number verify first time
      public function user_mobile_verify_post(){
        $output = array();
        // $user = (object)$this->input->post();
        $user = (object) json_decode($this->security->xss_clean( $this->input->raw_input_stream ), true);
        
        $output = array();
        $arr= array(
         array(
           'field'=>'mobile',
           'label'=>'',
           'rules'=>'required|min_length[10]|max_length[10]|regex_match[/^[0-9]{10}$/]|is_unique[tbl_user.mobile]',
           'errors'=>array('required'=>'mobile number is required','min_length'=>'Minimum length of mobile number is 10','max_length'=>'Max length of mobile number is 10','regex_match' => 'Please enter valid mobile number','is_unique'=>'ohh, given mobile number is already exists')
         )
       );
       $this->form_validation->set_rules($arr);
      //  print_r($this->form_validation->run());
      //  die();
       if ($this->form_validation->run()){
         $output['status'] = false;
         $output['msg'] = $this->form_validation->error_array()['mobile'];
        $this->set_response($output, REST_Controller::HTTP_OK); // CREATED (200) HTTP response code
      }else{
        $res = $this->custom->send_otp($user->mobile,$user->otp_type);
        $this->set_response($res, REST_Controller::HTTP_OK); // CREATED (200) being the HTTP response code
      }
    }

// Function for check backend login
public function backendLogin_post(){
  // $data = (object)$this->input->post();
  $data = (object) json_decode($this->security->xss_clean( $this->input->raw_input_stream ), true);
  $where_array = array('user_id'=>$data->user_id, 'user_token'=>$data->user_token, 'app_id'=>$data->app_id, 'mac_id'=>$data->mac_id);
  $res = $this->custom->checkExis("user_id, user_token, app_id, mac_id, user_status", "tbl_user", $where_array);
  $output = array();
  if ($res != NULL && count($res) > 0 && $res != 0) {
   $output['status'] = true;
   $output['msg'] = 'User activate now.!';
 }
  else{
   $output['status'] = false;
   $output['msg'] = 'User not valid.! De-activated.!';
  }
  $this->set_response($output, REST_Controller::HTTP_OK); // returns 200 OK response
}
    


}//class closing
?>