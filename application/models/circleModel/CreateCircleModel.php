<?php
class CreateCircleModel extends CI_Model {
  protected $date=null;
  public function __construct(){
    $this->load->database();
    $this->load->helper(array('form'));
    date_default_timezone_set('Asia/calcutta');
    $this->date=date('Y-m-d H:i:s');
    // $this->load->library('form_validation');
    // session_start();
    ob_start();
  }
  
  // Function for circle create
  public function circle_create($data){
    $this->db->insert('tbl_circle', $data);
    if ($this->db->affected_rows() > 0) {
      $circle_id = $this->db->insert_id();
      $data['circle_id'] = $circle_id;
      // Function for insert circle createtion role
      $role_details_arr = array('user_id'=> $data['created_by'],'created_by'=> $data['created_by'], 'role_id'=>2,'circle_id'=>$circle_id,'created_on'=>$this->date);
      $this->db->insert('tbl_role_details', $role_details_arr);
      if ($this->db->affected_rows() > 0) {
        $role_details_id = $this->db->insert_id();
        $res['status'] = true;
        $res['role_details_id'] = $role_details_id;
        $res['msg'] = "Role details added successfully.";
      return $data;
      }
      else{
        $this->db->where('circle_id', $circle_id);
        $this->db->delete('tbl_circle');
        return false;
      }
    }else{
      return false;
    }
  }



} // class closing
?>