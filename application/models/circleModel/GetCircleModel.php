<?php
class GetCircleModel extends CI_Model {

  function __construct()
  {
    // Construct the parent class
    parent::__construct();
    $this->load->model('circleModel/GetCircleModel','get');
    $this->load->model('commonModel/HelperModel','custom');
    $method = $_REQUEST;
    // $header = (object)$this->input->request_headers();
  }

  // Function for get all circles
  public function get_circle_list(){
    $this->db->select('c.circle_name, t.type_name, c.is_open, c.can_member_invite, c.vision, c.mission, c.purpose, c.created_on, c.created_by');
    $this->db->from('tbl_circle c');
    $this->db->join('tbl_type t', 't.type_id = c.type_id');
    $query = $this->db->get();
    $circle_data = $query->result_array();
    if($circle_data !=NULL && count($circle_data) > 0){
      return $circle_data;
    }else{
      return false;
    }
  }



  // Function for get circle and members of that circle by circle_id
  public function get_circle_by_circle_id($circle_id){
    $this->db->select('c.circle_name, t.type_name, c.is_open, c.circle_address, ct.city_name, c.locality, c.vision, c.mission, c.purpose, c.pin_code, c.created_on, c.created_by, u.f_name, u.m_name, u.l_name, u.mobile, u.email, ur.user_id, ur.role_id, ur.circle_id, ur.is_active');
    $this->db->from('tbl_circle c');
    $this->db->join('tbl_city ct', 'ct.city_id = c.city_id');
    $this->db->join('tbl_user u', 'u.user_id = c.created_by');
    $this->db->join('tbl_role_details ur', 'u.user_id = ur.user_id');
    $this->db->join('tbl_type t', 't.type_id = c.type_id');
    $this->db->where('c.circle_id',$circle_id);
    $query = $this->db->get();
    $circle_data = $query->result_array();
    if($circle_data !=NULL && count($circle_data) > 0){
      return $circle_data[0];
    }else{
      return false;
    }
  }

  //Function for get member details
    public function get_member_details($member_id){
      $this->db->select('u.user_id, u.invitation_id, u.f_name, u.m_name, u.l_name, u.address, c.city_name, u.locality, u.mobile, u.email, u.pin_code, u.app_id, u.mac_id, u.created_on');
      $this->db->from('tbl_user u');
      $this->db->join('tbl_city c','u.city_id = c.city_id');
      $this->db->where('u.user_id',$member_id);
        $query=$this->db->get();
        $member_data = $query->result_array();
      if ($member_data != NULL && count($member_data) > 0) {
        return $member_data;
      }else{
        return false;
      }
     }


  // SELECT rd.user_id, c.circle_name, c.is_open, c.can_member_invite, c.circle_address, c.locality, c.latitude, c.longitude,
  // c.pin_code, c.created_on from tbl_circle c join tbl_role_details rd ON c.circle_id = rd.circle_id where rd.user_id = 3

  // Function for get all circles with members
  public function get_circle_by_member_id($member_id){
    $this->db->select('rd.user_id, c.circle_id, c.circle_name, c.is_open, c.can_member_invite, c.created_on');
    $this->db->from('tbl_circle c'); 
    $this->db->join('tbl_role_details rd','c.circle_id = rd.circle_id');
    $this->db->where('rd.user_id',$member_id);
    $query=$this->db->get();
    $member_circle_data = $query->result_array();
    if ($member_circle_data != NULL && count($member_circle_data) > 0) {
      return $member_circle_data;
    }else{
      return false;
    }
   }

   
  

}//class closing