<?php
class CreateModel extends CI_Model {
  protected $date=null;
  public function __construct(){
    $this->load->database();
    $this->load->helper(array('form'));
    $this->load->library('form_validation');
    date_default_timezone_set('Asia/calcutta');
    $this->date=date('Y-m-d H:i:s');
    ob_start();
  }

  // Function for register user Insert data
  public function register_user($data){
    $this->db->insert('tbl_user', $data);
    if ($this->db->affected_rows() > 0) {
      $uid = $this->db->insert_id();
      $data['user_id'] = $uid;
      return $data;
    }else{
      return false;
    }
  }


} // class closing
?>