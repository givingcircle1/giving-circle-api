<?php
class GetModel extends CI_Model {
  protected $date=null;
  public function __construct(){
    $this->load->database();
    date_default_timezone_set('Asia/calcutta');
    $this->date=date('Y-m-d H:i:s');
  }

   // function for get login user
   public function user_login($data){
    $this->db->select('user_id');
    $this->db->from('tbl_user');
    $this->db->where($data);
    $query=$this->db->get();
    $user_data=$query->result_array();
    if ($user_data != NULL && count($user_data) > 0) {
      return $user_data;
    }else{
      return false;
    }
  }

  //Function for update OTP status
  public function otp_status_update($where_array,$update_array_mac_app,$number,$otp_type){
    $where_user=array('mobile' => $number);
    if($otp_type == 1){
    $this->db->select('user_id, user_token, f_name, m_name, l_name, email, mobile, user_status');
    // $this->db->select('user_id, user_token, f_name, m_name, l_name, email, mobile, DATE_FORMAT(created_on, "%a,%e %b,%h:%i %p") AS created_on');
    $this->db->from('tbl_user');
    $this->db->where($where_user);
    $query=$this->db->get();
    $user_data=$query->result_array();
    if ($user_data != NULL && count($user_data) > 0) {
      $this->db->where($where_array);
      $otp_status=array('status' => 1);
      $this->db->update('tbl_otp',$otp_status);
      $this->db->where($where_user);
      $this->db->update('tbl_user',$update_array_mac_app);
        return $user_data;
    }else{
      return false;
    }
  }else{
    //Update otp status
    $this->db->where($where_array);
    $otp_status=array('status' => 1);
    $this->db->update('tbl_otp',$otp_status);

    //Update user app_id & mac_id
    $this->db->where($where_user);
    $this->db->update('tbl_user',$update_array_mac_app);
    $data = array(1);
    return $data;
  }
  }
  //Function for get all users list
  public function get_user_list($circle_id){
    $this->db->select('u.user_id, u.invitation_id, u.user_token, u.f_name, u.m_name, u.l_name, u.gender, u.address, c.city_name, u.locality, u.mobile, u.email, u.dob, u.profession, u.pin_code, u.app_id, u.mac_id, u.created_on');
    $this->db->from('tbl_user u');
    $this->db->join('tbl_city c','u.city_id = c.city_id');
    if($circle_id != 0 && $circle_id != NULL){
      $this->db->join('tbl_role_details rd','rd.user_id = u.user_id');
      $this->db->where('rd.circle_id',$circle_id);
    }
    $query=$this->db->get();
    $user_data = $query->result_array();
    if ($user_data != NULL && count($user_data) > 0) {
      return $user_data;
    }else{
      return false;
    }
   }


   
  } // class closing
?>