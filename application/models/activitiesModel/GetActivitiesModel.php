<?php
class GetActivitiesModel extends CI_Model {

  function __construct()
  {
    // Construct the parent class
    parent::__construct();
    $this->load->model('activitiesModel/GetActivitiesModel','get');
    $this->load->model('commonModel/HelperModel','custom');
    $method = $_REQUEST;
    // $header = (object)$this->input->request_headers();
  }

    // Function for get circle and members of that circle by circle_id
    public function get_circle_by_id($circle_id){
      $this->db->select('c.circle_name, t.type_name, c.is_open, c.circle_address, ct.city_name, c.locality, c.vision, c.mission, c.purpose, c.pin_code, c.created_on, c.created_by, u.f_name, u.m_name, u.l_name, u.mobile, u.email, ur.user_id, ur.role_id, ur.circle_id, ur.is_active');
      $this->db->from('tbl_circle c');
      $this->db->join('tbl_city ct', 'ct.city_id = c.city_id');
      $this->db->join('tbl_user u', 'u.user_id = c.created_by');
      $this->db->join('tbl_role_details ur', 'u.user_id = ur.user_id');
      $this->db->join('tbl_type t', 't.type_id = c.type_id');
      $this->db->where('c.circle_id',$circle_id);
      $query = $this->db->get();
      $circle_data = $query->result_array();
      if($circle_data !=NULL && count($circle_data) > 0){
        return $circle_data[0];
      }else{
        return false;
      }
    }

  //Function for get activities by circle
  public function get_activities($circle_id){
    $this->db->select('a.activity_id, a.activity_name, a.circle_id, t.type_name, a.start_date, a.end_date, a.location, c.latitude, a.longitude, a.pin_code, a.need_collections, a.kind, a.time, a.money, a.created_on');
    $this->db->from('tbl_activity a');
    $this->db->join('tbl_type t','t.type_id = a.type_id');
    $this->db->join('tbl_circle c','c.circle_id = a.circle_id');
    $this->db->where('a.circle_id',$circle_id);
    $query=$this->db->get();
    $activity_data = $query->result_array();
    if ($activity_data != NULL && count($activity_data) > 0) {
      return $activity_data;
    }else{
      return false;
    }
   }
  


}//class closing
