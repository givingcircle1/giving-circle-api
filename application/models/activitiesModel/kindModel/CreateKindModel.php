<?php
class CreateKindModel extends CI_Model {
  protected $method = null;
  protected $date=null;
  function __construct()
  {
    // Construct the parent class
    parent::__construct();
    $this->load->model('activitiesModel/kindModel/CreateKindModel','material_create');
    $this->load->model('activitiesModel/kindModel/commonModel/HelperModel','custom');
    $this->method = $_REQUEST;
    $this->date=date('Y-m-d H:i:s');
  }
    
    // Function for add kind giving details
    public function add_kind($kind_data){
    $this->db->insert('tbl_kind_giving', $kind_data);
    if ($this->db->affected_rows() > 0) {
      $uid = $this->db->insert_id();
      $data['kg_id'] = $uid;
      return $data;
    }else{
      return false;
    }
}



}//class closing
