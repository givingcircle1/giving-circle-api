<?php
class CreateTimeModel extends CI_Model {
  protected $method = null;
  protected $date=null;
  function __construct()
  {
    // Construct the parent class
    parent::__construct();
    $this->load->model('activitiesModel/materialModel/CreateTimeModel','material_create');
    $this->load->model('activitiesModel/materialModel/commonModel/HelperModel','custom');
    $this->method = $_REQUEST;
    $this->date=date('Y-m-d H:i:s');
  }
    
    // Function for add time giving details
    public function add_time($time_data){
    $this->db->insert('tbl_time_giving', $time_data);
    if ($this->db->affected_rows() > 0) {
      $uid = $this->db->insert_id();
      $data['tg_id'] = $uid;
      return $data;
    }else{
      return false;
    }
}



}//class closing
