<?php
class CreateMoneyModel extends CI_Model {
  protected $method = null;
  protected $date=null;
  function __construct()
  {
    // Construct the parent class
    parent::__construct();
    $this->load->model('activitiesModel/moneyModel/CreateMoneyModel','money_create');
    $this->load->model('activitiesModel/moneyModel/commonModel/HelperModel','custom');
    $this->method = $_REQUEST;
    $this->date=date('Y-m-d H:i:s');
  }
    
    // Function for add money giving details
    public function add_money($money_data){
    $this->db->insert('tbl_money_giving', $money_data);
    if ($this->db->affected_rows() > 0) {
      $uid = $this->db->insert_id();
      $data['mg_id'] = $uid;
      return $data;
    }else{
      return false;
    }
}




}//class closing
