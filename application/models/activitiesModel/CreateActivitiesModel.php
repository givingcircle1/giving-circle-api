<?php
class CreateActivitiesModel extends CI_Model {
  protected $method = null;
  protected $date=null;
  function __construct()
  {
    // Construct the parent class
    parent::__construct();
    $this->load->model('activitiesModel/CreateActivitiesModel','create');
    $this->load->model('commonModel/HelperModel','custom');
    $this->method = $_REQUEST;
    $this->date=date('Y-m-d H:i:s');
  }

 // Function for add activity in circle by circle_id
  public function createNewActivity($data){
    $this->db->insert('tbl_activity', $data);
    if ($this->db->affected_rows() > 0) {
      $uid = $this->db->insert_id();
      return $uid;
    }else{
      return false;
    }
}

// Function add new collection by activity_id
  public function add_new_collection($data){
    $this->db->insert('tbl_collection', $data);
    if ($this->db->affected_rows() > 0) {
      $uid = $this->db->insert_id();
      return $uid;
    }else{
      return false;
    }
}



}//class closing
