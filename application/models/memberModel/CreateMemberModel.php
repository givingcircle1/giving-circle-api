<?php

class CreateMemberModel extends CI_Model {
  protected $method = null;
  protected $date=null;
  function __construct()
  {
    // Construct the parent class
    parent::__construct();
    $this->method = $_REQUEST;
    $this->date=date('Y-m-d H:i:s');
  }

  // Function for add members in circle
  public function add_members_in_circle($data){
      $role_details_arr = array('user_id'=> $data['user_id'], 'role_id'=>4,'circle_id'=>$data['circle_id'], 'is_active'=>$data['is_active'],'created_by'=> $data['created_by'], 'created_on'=>$this->date);
      
      $this->db->insert('tbl_role_details', $role_details_arr);
      if ($this->db->affected_rows() > 0) {
        $role_details_id = $this->db->insert_id();
        $res['status'] = true;
        $res['role_details_id'] = $role_details_id;
        $res['msg'] = "Member added successfully.";
        return $data;
      }
      else{
        return false;
        $res['msg'] = "Failed to add Member in circle.";
      }
  }
 


}//class closing
