<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

// Post file routes starts
// Create circle controller (Post Methods)
$route['register'] = 'commonController/CreateController/user_register';
$route['backend_login'] = 'commonController/CreateController/backendLogin';
$route['mobile_verify'] = 'commonController/CreateController/user_mobile_verify';
$route['circle_create'] = 'circleController/CreateCircleController/create_circle';
$route['add_member'] = 'memberController/CreateMemberController/add_member_in_circle';
$route['create_activity'] = 'activitiesController/CreateActivitiesController/create_activity';
$route['collection_add'] = 'activitiesController/CreateActivitiesController/add_collection';

// Get file routes starts (Get Method)
$route['login_user'] = 'commonController/GetController/log_send_user_otp';
$route['otp_verify'] = 'commonController/GetController/verify_otp';
$route['get_countries'] = 'commonController/GetController/country';
$route['get_states'] = 'commonController/GetController/states';
$route['get_cities'] = 'commonController/GetController/cities';
$route['get_all_user'] = 'commonController/GetController/all_user';
$route['get_all_circles'] = 'circleController/GetCircleController/all_circle';
$route['get_circle_by_member'] = 'circleController/GetCircleController/circle_by_member';
$route['get_members_by_circle'] = 'circleController/GetCircleController/members_by_circle';
$route['get_activities_by_circle'] = 'activitiesController/GetActivitiesController/activities_by_circle';

// Update file routes starts
// $route['update'] = 'UpdateController/update';
