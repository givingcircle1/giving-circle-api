<?php
class HelperModel extends CI_Model {
  protected $date=null;

  public function __construct(){
    $this->load->database();
    $this->date=date('Y-m-d H:i:s');

  }

// Generate Token
  public function genToken(){
    return bin2hex(openssl_random_pseudo_bytes(32));
  }

  // Function for check user exists or not
  public function checkExis($fields,$tbl,$where_array){
    $this->db->select($fields);
    $this->db->from($tbl);
    $this->db->where($where_array);
    $query=$this->db->get();
    $data=$query->result_array();
    if ($data != NULL && count($data) > 0) {
      return true;
    }else{
      return false;
    }
  }

  // Get data by passing parameters
  // $ifCon pass 1 for 1 record and pass 2 for more than 1 record.
  public function get_limited($fields, $table, $where_array, $ifCon){
    $this->db->select($fields);
    $this->db->from($table);
    $this->db->where($where_array);
    $query=$this->db->get();
    $res=$query->result_array();
    if (count($res) > 0) {
      $data = array();
      $condition = true;
      if ($ifCon == 1) {
        //here, 1 meance if only one record available then proceed
        if (count($res) > 1) {
          $condition = false;
        } else {
          $condition = true;
        }
      }
      if ($condition) {
        return $res;
      }else{
        return NULL;
      }
    } else {
      return NULL;
    }
  }

// Function for generate OTP
  function generate_otp(){
    return (string)substr( rand() * 900000 + 100000, 0, 4 );
  }

// Function for send OTP
  public function send_otp($number)
  {
    $today_date = date('Y-m-d');
    $arr = array('mobile_no'=> $number, 'DATE_FORMAT(created_on,"%Y-%m-%d")'=> $today_date);
    $otp_count = $this->get_limited('otp_no', 'tbl_otp', $arr, 2);
    if(count($otp_count) > 2){
      $res['status'] = false;
      $res['msg'] = "You have exceeded limit to send otp in one day.!";
      return json_encode($res); exit();
    }else{
      $res = array();
      if (!empty($number)) {
        $key = "fd2d5e61-2dda-11e9-8806-0200cd936042";
        $otp = $this->generate_otp();
        $this->url = "https://2factor.in/API/V1/".$key."/SMS/".$number."/".$otp;

        $curl = curl_init($this->url);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        $response = json_decode(curl_exec($curl));
        $err = curl_error($curl);
        curl_close($curl);
      // echo $response;
      // return $response;
        if ($err) {
          $res['status'] = false;
          $res['msg'] = $err;
          return $res; exit();
        }else if($response->Status){
          $otp_arr = array('otp_no'=> $otp, 'mobile_no'=>$number,'created_on'=>$this->date);
          $this->db->insert('tbl_otp', $otp_arr);
          if ($this->db->affected_rows() > 0) {
            $otp_id = $this->db->insert_id();
            $res['status'] = true;
            $res['otp_id'] = $otp_id;
            $res['msg'] = "OTP has been successfully sent to ".$number."";
             return $res; exit();          
          }else{
          $res['status'] = false;
          $res['msg'] = "Oop's..Something wents wrong, Please try again.!";
          return $res; exit();          
        }
          return $res; exit();
        }else{
          $res['status'] = false;
          $res['msg'] = "Failed to send OTP.!";
          return $res; exit();
        }
      }else{
        $res['status'] = false;
        $res['msg'] = "Please Provoide valid mobile number.!";
        return $res; exit();
      }
    }
} // function closing


}// class closing
?>
